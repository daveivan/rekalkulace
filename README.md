# Rekalkulace

## Spuštění

Jak dostat kód:

```
git clone https://gitlab.com/daveivan/rekalkulace.git
```

tím se vytvoří složka `rekalkulace`, ve které se bude všechno odehrávat.

Poprvé je třeba instalovat závislosti

```
npm run install
```

Pak stačí spustit

```
npm run dev
```

tím se zkompiluje kód do JS a spustí se server na adrese

```
http://localhost:5000/
```

## Jak dělat změny

Po změnách by bylo ideální to nahrávat na gitlab, ať se udržuje jedna aktuální verze.

1. Provedou se změny v kódu
2. `git add .`
3. `git commit -m "nazev upravy"`
4. `git push origin master`

## Demo

Po pushnutí aktuální verze se na stránce https://gitlab.com/daveivan/rekalkulace/-/pipelines po dokončení všech "puntíků" v poslední pipeline může ručně spustit poslední "puntík" `pages`.

![](pipeline.png)

tím se nahraje aktuální verze na stránku

- https://gitlab.com/daveivan/rekalkulace/pages

## Testy

Všechny funkce co něco počítají jsou pokryty testy, které se spouštění pomocí

```
npm run test
```

Pokud se něco ve výpočtech změní, testy ověří, zda změna něco nerozbila. Pokud změny rozbíjí testy, je třeba výpočty prověřit a pŕípadně upravit testy.

## Provoz

Minifikovaná verze se vytvoří pomocí

```
npm run build
```

následně se obsah složky `public` zkopíruje na server. Je možné to využít tak jak je, nebo si to vložit na vlastní stránky. K tomu je potřeba vložit do html

- `build/bundle.js`
- `build/bundle.css`
- `global.css`
- `bootstrap.min.css`

Pro ukázku viz `index.html` ve složce `public`.

Aplikace se může vkládat do libovolného prvku (např. nějaký div na již existující stránce), stačí jen změnit `target` v souboru `main.ts`.

## Komponenty

- Hlavní komponenta se nachází v souboru `Calculator.svelte`, kde jsou všechny prvky umístěny
- Prvky jsou rozděleny pro zpřehlednění do několika komponent
- Komponenty pro inputy: `DateInput.svelte`, `SelectInput.svelte` a `TextInput.svelte`
- Komponenta pro text: `TextField.svelte`
- Tyto komponenty jsou obaleny `InputWrapper.svelte`, který definuje `label` a strukturu inputu.
- Komponenta `Zones.svelte` zapouzdřuje "pásma" a má 2 varianty: "Přejeté" a "Nedojeté" kilometry. Daná varianta se zobrazí podle hodnoty "Rozdíl v nájezdech"

## Formulář

- Využívá bootstrap (v5)
- Jednotlivé inputy jde použít ve dvou variantách:

  1. label a input pod sebou
  2. label a input vedle sebe (stačí uvést atribut `input` (nebo `input={true}`))

- Obalovacímu divu inputu lze nastavit třídu pomocí `customClass`
- Další možnosti konfigurace jednotlivých inputů viz kód

## CSS

- Celá aplikace je obalena třídou `.recalc`.
- Je využíván bootstrap, který se nachází ve složce `public`
- V souboru `public/global.css` se nacházejí styly pro celou aplikaci, změny zde provedné budou mít vliv na všechny komponenty. Zde se změny provádějí přímo.
- Je možné psát css i přímo ve `.svelte` komponentách do tagu `<style></style>`. Tyto styly se aplikují jen na danou komponentu a neovlivní jiné prvky se stejnou třídou, které jsou v jiné komponentě. Všechny tyto `<style>` ve všech komponentách se následně spojí při kompilaci do souboru `build/bundle.css`. Tento soubor se ručně neupravuje.
- Proto je třeba mít v html stránce jak `public/global.css`, tak i `public/build/bundle.css`

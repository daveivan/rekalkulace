import {
    getAvgMonthMileage,
    getExpectedMileage,
    getDiffMileage,
    getRecalculate,
    getAdditionalPayment,
    computeFromKm,
    getCostsWithRecalculation,
    getCosts,
    getContractExpiredNew
} from './service';
import {RecalculateOption} from './enums';

describe('service', () => {
    describe('getAvgMonthMileage', () => {
        it('should return result', () => {
            expect(getAvgMonthMileage(125000, '2018-10-10', '2020-08-31')).toBe(5502.29136517125);
        });

        it('should return null if dates is not provided', () => {
            expect(getAvgMonthMileage(125000, '', 'invalid date')).toBe(null);
            expect(getAvgMonthMileage(125000, '2020-11', 'null')).toBe(null);
        });
    });

    describe('getExpectedMileage', () => {
        it('should return result', () => {
            expect(getExpectedMileage(125000, 5502.29136517125, 13.28)).toBe(198070.4293294742);
        });
    });

    describe('getDiffMileage', () => {
        it('should return result', () => {
            expect(getDiffMileage(198082, 180000)).toBe(18082);
        });
    });

    describe('getRecalculate', () => {
        it('should return true', () => {
            expect(getRecalculate(200000, 180000, 50000, 20000)).toBe('Ano');
            expect(getRecalculate(200000, 180000, -50000, 20000)).toBe('Ano');
        });

        it('should return false', () => {
            expect(getRecalculate(200000, 180000, 50000, 50001)).toBe('Ne');
            expect(getRecalculate(200000, 180000, -50000, 50001)).toBe('Ne');
        });

        it('should return true (max)', () => {
            expect(getRecalculate(200000, 210000, 0, 0)).toBe('Ano - km přes maximální nájezd');
        });
    });

    describe('getAdditionalPayment', () => {
        it('should return result', () => {
            expect(getAdditionalPayment(18082, 3001, 10000, 1.5)).toBe(10498.5);
        });

        it('should return result if zoneTo si not provided', () => {
            expect(getAdditionalPayment(18082.48915, 10001, null, 3)).toBe(24244.467450000004);
        });

        it('should return 0', () => {
            expect(getAdditionalPayment(18082.48915, 19000, null, 3)).toBe(0);
        });
    });

    describe('computeFromKm', () => {
        it('should return from km array', () => {
            expect(computeFromKm([3000, 10000, 20000])).toStrictEqual([0,3001,10001]);
            expect(computeFromKm([0])).toStrictEqual([0]);
            expect(computeFromKm([])).toStrictEqual([0]);
        });
    });

    describe('getCostsWithRecalculation', () => {
        it('should return result if origin contract expired date is newer than new expired date', () => {
            expect(getCostsWithRecalculation('2020-10-01', 15000, '2021-10-09', '2021-09-01', 13000)).toBe(181446.57534246577);
        });

        it('should return result if origin expired date is same or older than new expired date', () => {
            expect(getCostsWithRecalculation('2020-10-01', 15000, '2021-10-09', '2021-10-09', 13000)).toBe(183945.20547945204);
            expect(getCostsWithRecalculation('2020-10-01', 15000, '2021-10-01', '2021-10-09', 13000)).toBe(183945.20547945204);
        });
    });

    describe('getCosts', () => {
        it('should return result if origin expired date is newer or same than new expired date', () => {
            expect(getCosts('2021-10-09', '2021-10-09', '2020-10-01', 12350, 34742.96744, 13000)).toBe(186191.1866180822);
            expect(getCosts('2021-10-31', '2021-10-09', '2020-10-01', 12350, 46682.18596, 13000)).toBe(207063.0078778082);
        });

        it('should return result if origin expired date is older than new expired date', () => {
            expect(getCosts('2021-10-09', '2021-10-10', '2020-10-01', 12350, 34742.96744, 13000)).toBe(186618.58387835618);
        });
    });

    describe('getContractExpiredNew', () => {
        it('should return result if maxMileage is greather then expected mileage', () => {
            expect(getContractExpiredNew(RecalculateOption.Yes, '2021-06-01', 200000, 125000, 5000, '2021-01-01')).toBe('2021-06-01');
        });

        it('should return result if maxMileage is smaller then expected mileage', () => {
            expect(getContractExpiredNew(RecalculateOption.YesMaxMileage, '2021-06-01', 200000, 180000, 10000, '2021-01-01')).toBe('2021-03-01');
        });
    });
});
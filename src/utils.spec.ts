import {
  getDiffInDays,
  isNewerOrSameThen,
  isNewerThen,
  getNewerDate,
  getMonths,
  formatValue,
  formatDate,
  getDaysFromMonths,
} from "./utils";

describe('utils', () => {
    describe('isNewerOrSameThen', () => {
        it('should return true', () => {
            expect(isNewerOrSameThen('2020-02-02', '2020-02-01')).toBe(true);
            expect(isNewerOrSameThen('2020-02-02', '2020-02-02')).toBe(true);
        });

        it('should return false', () => {
            expect(isNewerOrSameThen('2020-02-01', '2020-02-02')).toBe(false);
            expect(isNewerOrSameThen('invalid', '2020-02-02')).toBe(false);
            expect(isNewerOrSameThen('invalid', 'invalid')).toBe(false);
        });
    });

    describe('isNewerThen', () => {
        it('should return true', () => {
            expect(isNewerThen('2020-02-02', '2020-02-01')).toBe(true);
        });

        it('should return false', () => {
            expect(isNewerThen('2020-02-01', '2020-02-02')).toBe(false);
            expect(isNewerThen('invalid', '2020-02-02')).toBe(false);
            expect(isNewerThen('2020-02-02', 'invalid')).toBe(false);
            expect(isNewerThen('invalid', 'invalid')).toBe(false);
            expect(isNewerThen('2020-02-02', '2020-02-02')).toBe(false);
        });
    });

    describe('getNewerDate', () => {
        it('should return newer date', () => {
            expect(getNewerDate('2020-01-01', '2020-02-02')).toBe('2020-02-02');
            expect(getNewerDate('2020-01-01', 'invalid')).toBe('2020-01-01');
            expect(getNewerDate('invalid', '2020-02-02')).toBe('2020-02-02');
        });

        it('should return null', () => {
            expect(getNewerDate('invalid', 'invalid')).toBeNull();
        });
    });

    describe('getDiffInDays', () => {
        it('should return result', () => {
            expect(getDiffInDays('2020-01-01', '2020-01-10')).toBe(9);
            expect(getDiffInDays('2020-01-01', '2020-01-01')).toBe(0);
            expect(getDiffInDays('2020-01-10', '2020-01-01')).toBe(-9);
        });

        it('should return NaN', () => {
            expect(getDiffInDays('1.1.2020', '2020-01-10')).toBeNaN();
            expect(getDiffInDays('', '2020-01-10')).toBeNaN();
        });
    });

    describe('getMonths', () => {
        it('should return result', () => {
            expect(getMonths('2020-01-01', '2020-02-01')).toBe(1.0191780821917809);
            expect(getMonths('2020-01-01', '2020-01-01')).toBe(0);
        });

        it('should return NaN', () => {
            expect(getMonths('invalid', '2020-01-01')).toBeNaN();
        });
    });

    describe('formatValue', () => {
        it('should return formatted value', () => {
            expect(formatValue(12)).toBe('12');
            expect(formatValue(1222)).toBe('1 222');
            expect(formatValue(12223)).toBe('12 223');
            expect(formatValue(122234)).toBe('122 234');
        });

        it('should return rounded formatted value', () => {
            expect(formatValue(12, 2)).toBe('12');
            expect(formatValue(12.3444, 2)).toBe('12,34');
            expect(formatValue(12.34456, 4)).toBe('12,3446');
            expect(formatValue(1222222.39999, 2)).toBe('1 222 222,4');
        });

        it('should return formatted value with suffix', () => {
            expect(formatValue(12, 0, 'Kč')).toBe('12 Kč');
            expect(formatValue(12345, 0, 'Km')).toBe('12 345 Km');
            expect(formatValue(12345.66, 1, 'Km')).toBe('12 345,7 Km');
        });

        it('should return empty value', () => {
            expect(formatValue(undefined)).toBe('-');
            expect(formatValue(NaN)).toBe('-');
            expect(formatValue(null)).toBe('-');
        })
    });

    describe('formatDate', () => {
        it('should format date', () => {
            expect(formatDate('2020-01-02')).toBe('2. 1. 2020');
        });

        it('should return empty value', () => {
            expect(formatDate('invalid')).toBe('');
        });
    });

    describe('getDaysFromMonths', () => {
        it('should return result', () => {
            expect(getDaysFromMonths(0)).toBe(0);
            expect(getDaysFromMonths(1)).toBe(30.416666666666668);
        });
    });
});
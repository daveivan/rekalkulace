export enum DiffMileageUnit {
    Km = 'km',
    Percent = '%',
};

export enum FutureMileageCalcMode {
    Computed = 'Aktuální nájezd',
    Custom = 'Vlastní',
};

export enum RecalculateOption {
    No = 'Ne',
    Yes = 'Ano',
    YesMaxMileage = 'Ano - km přes maximální nájezd',
}
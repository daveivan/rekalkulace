import { differenceInCalendarDays, parseISO } from "date-fns";
import format from "number-format.js";
import isNil from "lodash/isNil";
import isNaN from "lodash/isNaN";

/**
 * Zjisti, zda je datum `from` novejsi nebo stejne nez datum `to
 * @param from Datum od (ve formatu YYYY-MM-DD)
 * @param to Datum do (ve formatu YYYY-MM-DD)
 * @returns boolean
 */
export const isNewerOrSameThen = (from: string, to: string) => {
  return new Date(from) >= new Date(to);
};

/**
 * Zjisti, zda je datum `from` novejsi nez datum `to
 * @param from Datum od (ve formatu YYYY-MM-DD)
 * @param to Datum do (ve formatu YYYY-MM-DD)
 * @return boolean
 */
export const isNewerThen = (from: string, to: string) => {
  return new Date(from) > new Date(to);
};

/**
 * Zjisti, zda je datum `first` novejsi nez datum `second`
 * @param first Datum od (ve formatu YYYY-MM-DD)
 * @param second Datum do (ve formatu YYYY-MM-DD)
 * @return boolean
 */
export const isSame = (first: string, second: string) => {
  const firstDate = new Date(first);
  const secondDate = new Date(second);

  return (
    firstDate.getFullYear() === secondDate.getFullYear() &&
    firstDate.getMonth() === secondDate.getMonth() &&
    firstDate.getDate() === secondDate.getDate()
  );
};

/**
 * Najdi novejsi datum
 * @param a datum (ve formatu YYYY-MM-DD)
 * @param b datum (ve formatu YYYY-MM-DD)
 * @returns novejsi datum ve tvaru 'YYYY-MM-DD' nebo null
 */
export const getNewerDate = (a: string, b: string) => {
  const aDate = new Date(a);
  const bDate = new Date(b);

  if (!aDate.valueOf() && bDate.valueOf()) return b;
  if (!bDate.valueOf() && aDate.valueOf()) return a;
  if (!aDate.valueOf() && !bDate.valueOf()) return null;

  return aDate > bDate ? a : b;
};

/**
 * Pocet dnu mezi zadanymi datumy
 * @param dateFrom Datum od (ve formatu YYYY-MM-DD)
 * @param dateTo Datum do (ve formatu YYYY-MM-DD)
 * @returns Pocet dnu
 */
export const getDiffInDays = (dateFrom: string, dateTo: string) => {
  return differenceInCalendarDays(parseISO(dateTo), parseISO(dateFrom));
};

/**
 * Vrati pocet mesicu v desetinnem formatu
 * @param dateFrom Datum od (ve formatu YYYY-MM-DD)
 * @param dateTo Datum do (ve formatu YYYY-MM-DD)
 * @returns Pocet mesicu (pr. 13,24)
 */
export const getMonths = (dateFrom: string, dateTo: string) => {
  const days = getDiffInDays(dateFrom, dateTo);

  return days / (365 / 12);
};

/**
 * Vrati pocet dni z poctu mesicu
 * @param months Pocet mesicu v desetinnem formatu
 */
export const getDaysFromMonths = (months: number) => {
  return months * (365 / 12);
};

/**
 *
 * @param value Hodnota
 * @param round Zaokrouhlit hodnotu na dany pocet mist
 * @param suffix Symbol za hodnotou
 * @returns Formatovana hodnota
 */
export const formatValue = (value?: number | null, round = 0, suffix = "") => {
  if (isNil(value) || isNaN(value)) {
    return "-";
  }

  let mask = "# ##0,";

  if (round) {
    mask += "#".repeat(round);
  }

  if (suffix) {
    mask += " " + suffix;
  }

  return format(mask, value);
};

export const formatDate = (date: string) => {
  const d = new Date(date);

  if (!d.valueOf()) {
    return "";
  }

  const formattedDate = `${d.getDate()}. ${
    d.getMonth() + 1
  }. ${d.getFullYear()}`;

  return formattedDate;
};

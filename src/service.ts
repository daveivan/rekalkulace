import moment from "moment";
import {
  isNewerOrSameThen,
  getNewerDate,
  getDiffInDays,
  getMonths,
  getDaysFromMonths,
} from "./utils";
import { RecalculateOption } from "./enums";

/**
 * Prumerne mesicni najezdy celkem
 * @param mileage Aktualni najezd (km)
 * @param contractValidFrom Datum od kdy plati smlouva (ve formatu YYYY-MM-DD)
 * @param currentMileageDate Najezd aktualni k datu (ve formatu YYYY-MM-DD)
 * @returns Avg month mileage (km)
 *
 */
export const getAvgMonthMileage = (
  mileage: number,
  contractValidFrom: string,
  currentMileageDate: string
) => {
  const months = getMonths(contractValidFrom, currentMileageDate);

  if (!months) {
    return null;
  }

  return mileage / months;
};

/**
 * Predpokladany najezd
 * @param mileage Aktualni najezd (km)
 * @param avgMonthMileage Prumerny mesicni najezd (km)
 * @param contractExpired Zbyva do konce smlouvy (mesice)
 * @returns Expected mileage (km)
 *
 */
export const getExpectedMileage = (
  mileage: number,
  avgMonthMileage: number,
  contractExpired: number
) => {
  return mileage + avgMonthMileage * contractExpired;
};

/**
 * Rozdil v najezdech
 * @param expectedMileage Predpokladany najezd
 * @param contractMileage Smluveny najezd
 * @returns Rozdil v najezdech
 */
export const getDiffMileage = (
  expectedMileage: number,
  contractMileage: number
) => {
  return expectedMileage - contractMileage;
};

/**
 * Rekalkulovat?
 * @param maxMileage Maximalni najezd (km)
 * @param expectedMileage Predpokladany najezd (km)
 * @param diffMileage Rozdil v najezdech (km|%)
 * @param recalculateThreshold Rekalkulovat pri rozdilu v najezdech (km|%)
 * @returns Je treba rekalkulovat?
 */
export const getRecalculate = (
  maxMileage: number,
  expectedMileage: number,
  diffMileage: number,
  recalculateThreshold: number
) => {
  if (expectedMileage > maxMileage) {
    return RecalculateOption.YesMaxMileage;
  }

  if (Math.abs(diffMileage) > recalculateThreshold) {
    return RecalculateOption.Yes;
  }

  return RecalculateOption.No;
};

/**
 * Vypocet doplatku
 * @param diffMileage Rozdil v najezdech (km)
 * @param zoneFrom Prejete km od (km)
 * @param zoneTo Prejete km do (km|null)
 * @param price Cena (kc)
 * @returns Doplatek za dane pasmo
 */
export const getAdditionalPayment = (
  diffMileage: number,
  zoneFrom: number,
  zoneTo: number | null,
  price: number
) => {
  diffMileage = Math.abs(diffMileage);

  if (zoneTo && diffMileage > zoneTo) {
    const km = zoneTo || diffMileage;

    return (km - zoneFrom) * price;
  } else {
    const km = diffMileage - zoneFrom;

    if (km >= 0) {
      return km * price;
    } else {
      return 0;
    }
  }
};

/**
 * Vypocti pole "od" km
 * @param toArray Pole "do" km
 * @returns Pole "od"
 */
export const computeFromKm = (toArray: number[]) => {
  const fromKm = [0];

  for (let i = 0; i < toArray.length - 1; i++) {
    fromKm.push(toArray[i] ? toArray[i] + 1 : 0);
  }

  return fromKm;
};

/**
 * Vypocte naklady s rekalkulaci
 * @param recalculateFrom Rekalkulace od data (Pro vypocet Zbyva do konce smlouvy (novy parametr))
 * @param monthlyRentNew Mesicni najemne (novy parametr)
 * @param contractExpired Datum konce smlouvy
 * @param contractExpiredNew Datum konce smlouvy (novy parametr)
 * @param monthlyRentNewCar Mesicni najemne - nove vozidlo
 * @returns Naklady s rekalkulaci
 */
export const getCostsWithRecalculation = (
  recalculateFrom,
  monthlyRentNew,
  contractExpired,
  contractExpiredNew,
  monthlyRentNewCar
) => {
  const contractRemainingMonthsNew = getMonths(
    recalculateFrom,
    contractExpiredNew
  );
  let result = contractRemainingMonthsNew * monthlyRentNew;

  const diffExpired = getDiffInDays(contractExpiredNew, contractExpired);

  if (diffExpired > 0) {
    result += (diffExpired / (365 / 12)) * monthlyRentNewCar;
  }

  return result;
};

/**
 *
 * @param contractExpired Datum konce smlouvy
 * @param contractExpiredNew Datum konce smlouvy (novy parametr)
 * @param recalculateFrom Rekalkulace od data
 * @param monthlyRent Mesicni najemne
 * @param surchargeOrReturnSum Vraceni nebo doplatek za prejete nebo nedojete km
 * @param monthlyRentNewCar Mesicni najemne - nove vozidlo
 * @returns Naklady bez rekalkulace
 */
export const getCosts = (
  contractExpired,
  contractExpiredNew,
  recalculateFrom,
  monthlyRent,
  surchargeOrReturnSum,
  monthlyRentNewCar
) => {
  const months = getDiffInDays(recalculateFrom, contractExpired) / (365 / 12);

  let result = months * monthlyRent + surchargeOrReturnSum;

  if (isNewerOrSameThen(contractExpired, contractExpiredNew)) {
    return result;
  } else {
    const date = getNewerDate(contractExpired, contractExpiredNew);
    const m = getDiffInDays(contractExpired, date) / (365 / 12);
    result += m * monthlyRentNewCar;

    return result;
  }
};

/**
 * Vypocte nove datum konce smlouvy podle novych pozadavku z emailu
 * @param recalculateState Stav rekalkulace
 * @param contractExpired Datum konce smlouvy (stavajici vozidlo)
 * @param maxMileage Maximalni najezd
 * @param currentMileage Aktualni najezd
 * @param avgMonthlyMileage Prumerne mesicni najezdy (Aktualni najezd nebo vlastni)
 * @param currentMileageDate Najezd k datu
 * @returns Datum konce smlouvy (pro nove parametry smlouvy)
 */
export const getContractExpiredNew = (
  recalculateState,
  contractExpired,
  maxMileage,
  currentMileage,
  avgMonthlyMileage,
  currentMileageDate
) => {
  if (recalculateState === RecalculateOption.YesMaxMileage) {
    // Maximalni najezd je mensi nez predpokladany najezd, vypocti nove datum konce smlouvy
    const monthsToMaxMileage =
      (maxMileage - currentMileage) / avgMonthlyMileage;
    const daysToMaxMileage = Math.floor(getDaysFromMonths(monthsToMaxMileage));

    const contractExpiredNew = moment(currentMileageDate)
      .add(daysToMaxMileage - 1, "days")
      .format("YYYY-MM-DD");

    return contractExpiredNew;
  } else {
    // Maximalni najezd je vetsi nez predpokladny najezd, jen zkopiruj datum konce smlouvy stavajiciho vozidla
    return contractExpired;
  }
};
